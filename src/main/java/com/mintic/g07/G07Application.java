package com.mintic.g07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G07Application {

	public static void main(String[] args) {
		SpringApplication.run(G07Application.class, args);
	}

}
